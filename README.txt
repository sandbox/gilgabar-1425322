Washington State Sales Tax for Drupal Commerce
----------------------------------------------
This module provides Washington State destination-based sales tax calculation for Drupal Commerce. The state of Washington requires sales tax to be calculated based on the destination to which a product is delivered rather than the location of the business selling the product. Because the sales tax is a combination of a state sales tax plus county and city sales taxes (which vary according to the county or city) it is not possible to use a fixed rate set with Commerce Tax to calculate the sales tax for Washington locations. This module uses a web service provided by the Washington Department of Revenue that provides the appropriate tax rate given a particular address. The rate is then passed to Commerce Tax by a Rules action, so you will still have all the flexibility of the Rules-based Commerce Tax module to set conditions and apply taxes as required by your situation.

Installation
------------
- Install this module as you would any other module.
- Visit admin/commerce/config/taxes/rates
- You should see the 'Washington Sales Tax' rate in the list.  You may customize the rate by using the 'edit' and 'configure component' links.
  - The 'display title' controls the name of the tax as displayed in a line item.
  - The 'rate' specified on the edit page will be used as a default if the DOR service is unavailable or unable to resolve an address.
  - You may add conditions to the Rules component if you wish to customize how and to what the tax is applied.

Usage
-----
Once installed usage should be transparent. As soon as Drupal Commerce has a shipping address it will add a 'Washington sales tax' line item to an order if that address is in Washington.

Applying Sales Tax to Non-Product Line Items
--------------------------------------------
Washington requires sales tax to be calculated on shipping charges. By default Drupal Commerce will not calculate sales tax for shipping line items. It is easy, though not necessarily obvious how, to make it do so:

- Make sure you have installed the Commerce Shipping module
- Visit admin/config/workflow/rules
- Edit the rule 'Calculate taxes: Sales tax'
- Add a new event: 'Calculating a shipping rate'

That's it! Now Drupal Commerce will calculate taxes for shipping line items when it calculates shipping. 

If you have more complicated tax calculation needs then you may be able to use the conditions of the 'Calculate taxes: Sales tax' rule or the conditions of the tax rate component to specify those exceptions. If you have further questions about configuring custom conditions the issue queue of the Commerce module, which contains the Commerce Tax module, may be the best place to get answers.

Disclaimer
----------
The author of this module is neither an accountant nor a tax attorney and is actually quite naive when it comes to dealing with taxes and other legal matters. You should not assume that anything written here or that this module does is correct. You are strongly encouraged to have an actual tax professional review your usage of this module to be sure you are doing everything legally required. Please file issues if you have any corrections.

Credits
-------
Created by Brendan Andersen (gilgabar)
Thanks to Jennifer Hodgdon (jhodgdon) for the Washington State Sales Tax for Ubercart module on which this module is partially based.
