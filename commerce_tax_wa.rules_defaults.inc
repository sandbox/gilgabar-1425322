<?php

/**
 * @file
 * Defines a tax component and rules.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_tax_wa_default_rules_configuration() {
  $rules = array();
  $name = 'wa_sales_tax';
  $tax_rate = commerce_tax_rate_load($name);

  $rule = rule(commerce_tax_rate_component_variables());
  $rule->label = t('Calculate @title', array('@title' => $tax_rate['title']));

  // Make sure the shipping address is in the US.
  $rule
    ->condition('commerce_order_compare_address', array(
    'commerce_order:select' => 'commerce-line-item:order',
    'address_field' => 'commerce_customer_shipping|commerce_customer_address',
    'address_component' => 'country',
    'value' => 'US',
  ));

  // Make sure the shipping address is in WA.
  $rule
    ->condition('commerce_order_compare_address', array(
    'commerce_order:select' => 'commerce-line-item:order',
    'address_field' => 'commerce_customer_shipping|commerce_customer_address',
    'address_component' => 'administrative_area',
    'value' => 'WA',
  ));

  // Add the action to apply the WA sales tax.
  $rule
    ->action('commerce_tax_wa_rate_apply', array(
      'commerce_line_item:select' => 'commerce-line-item',
      'tax_rate_name' => $name,
    ));

  $rules[$tax_rate['rules_component']] = $rule;

  return $rules;
}
